package com.crud.crud;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
@Entity
public class Type_fichier implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	
	@Column
	private int id;
	@Column
	private String type_fichier;
	
	
	public Type_fichier() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Type_fichier(String type_fichier) {
		super();
		this.type_fichier = type_fichier;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType_fichier() {
		return type_fichier;
	}
	public void setType_fichier(String type_fichier) {
		this.type_fichier = type_fichier;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
