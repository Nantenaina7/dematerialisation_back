package com.crud.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.crud.crud.dao.Type_userRepository;
import com.crud.crud.dao.UtilisateurRepository;



@SpringBootApplication
public class DematerialisationApplication {

	public static void main(String[] args) {
		ApplicationContext ctx= SpringApplication.run(DematerialisationApplication.class, args);
	}
}
