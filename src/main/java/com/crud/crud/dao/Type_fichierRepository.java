package com.crud.crud.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.crud.Service;
import com.crud.crud.Type_fichier;
public interface Type_fichierRepository extends JpaRepository <Type_fichier, Integer>{

}
