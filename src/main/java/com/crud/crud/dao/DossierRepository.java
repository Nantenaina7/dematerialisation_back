package com.crud.crud.dao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.crud.crud.Dossier;
public interface DossierRepository extends JpaRepository<Dossier, Integer> {

//	List<Dossier> findBytitre(String titre);

//	List<Dossier> findByTitreDossierByCheminDossier(String titreDossier, String classement);
}
