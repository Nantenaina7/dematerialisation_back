package com.crud.crud.dao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.crud.crud.Utilisateurs;

public interface UtilisateurRepository  extends JpaRepository < Utilisateurs,Integer>{
	//List<Utilisateurs> findByNomUtilisateurs(String nom);
	@Query("select u from Utilisateurs u where u.nom like :x")
	public List<Utilisateurs> findlikeNom(@Param("x")String mc); 
	  

}
