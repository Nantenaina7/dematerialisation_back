package com.crud.crud.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.crud.Type_fichier;
import com.crud.crud.Type_user;
public interface Type_userRepository extends JpaRepository <Type_user, Integer>{

}
