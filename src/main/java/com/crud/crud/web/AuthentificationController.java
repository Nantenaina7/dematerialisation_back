package com.crud.crud.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.crud.crud.Utilisateurs;
import com.crud.crud.dao.UtilisateurRepository;

@Controller
@CrossOrigin("*")
@RequestMapping("/dematerialisation")
public class AuthentificationController {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@PostMapping(value="/authentification")
	public ResponseEntity<Utilisateurs> ajout(@RequestParam String email, @RequestParam String password) {
		List<Utilisateurs> user = utilisateurRepository.findAll();
		for(Utilisateurs u : user) {
			if(u.getMail().equals(email) && u.getMot_de_passe_identifiant().equals(password)) {
		        return new ResponseEntity<Utilisateurs>(u, HttpStatus.OK);
			}
		}
		return null;
	}
}
