package com.crud.crud.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.crud.crud.Dossier;
import com.crud.crud.Utilisateurs;
import com.crud.crud.dao.DossierRepository;
import com.crud.crud.dao.UtilisateurRepository;

@Controller
@CrossOrigin("*")
@RequestMapping("/dematerialisation/dossier")
public class DossierController {
	
	
	@PostMapping(value="/ajoutDossier")
	public ResponseEntity<List<Dossier>> ajout(@RequestBody Dossier dossier) {
		
		this.dossierRepository.save(dossier);
		List<Dossier>lsdossier=dossierRepository.findAll();
		return new ResponseEntity<List<Dossier>>(lsdossier, HttpStatus.OK);	
	}

	@Autowired
	private DossierRepository dossierRepository;
	@RequestMapping(value="/listeDossier")
	public ResponseEntity<List<Dossier>> listeDossier() {
		List<Dossier>dossier=dossierRepository.findAll();
		return new ResponseEntity<List<Dossier>>(dossier, HttpStatus.OK);	
	}
	
	@RequestMapping(value="/supprimerDossier")
	public ResponseEntity<List<Dossier>> supprimer(Integer id) {
		Dossier dossier=new Dossier();
		dossier.setId(id);
		this.dossierRepository.delete(dossier);
		List<Dossier>lsdossier=dossierRepository.findAll();
		return new ResponseEntity<List<Dossier>>(lsdossier, HttpStatus.OK);	
	}
	
	@RequestMapping(value="/saveDossier")
	public ResponseEntity<List<Dossier>> modifier(@RequestParam Dossier dossier) {
		this.dossierRepository.save(dossier);
		List<Dossier>lsdossier=dossierRepository.findAll();
		return new ResponseEntity<List<Dossier>>(lsdossier, HttpStatus.OK);	
	}
	
	@RequestMapping(value="/rechercheDossier")
	public ResponseEntity<List<Dossier>> rechercherDossier(@RequestParam(name = "titreDossier",defaultValue = "classement")String titreDossie ,@RequestParam(name="im",defaultValue = "0" )Integer im) {
		
		List<Dossier> lsdossier= this.dossierRepository.findAll();
		return new ResponseEntity<List<Dossier>>(lsdossier, HttpStatus.OK);	
}
}
