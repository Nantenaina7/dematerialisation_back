package com.crud.crud.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.crud.crud.Fichier;
import com.crud.crud.Utilisateurs;
import com.crud.crud.dao.FichierRepository;

import utils.MediaTypeUtils;



@Controller
@CrossOrigin("*")
@RequestMapping("/dematerialisation")
public class FichierController {
	private static final Logger logger = Logger.getLogger(FichierController.class.getName());
	private static String UPLOADED_FOLDER = "C:\\Users\\asus112\\Documents\\workspace-spring-tool-suite-4-4.3.2.RELEASE\\dematerialisation\\src\\pdf\\";
	
	@Autowired
	private FichierRepository fichierRepository;
	@Autowired
    private ServletContext servletContext;
	
	@RequestMapping(value="/fichier")
	public ResponseEntity<List<Fichier>> liste(Model model) {
		List<Fichier>fichier=fichierRepository.findAll();
		model.addAttribute("ListeFichier", fichier);
		return new ResponseEntity<List<Fichier>>(fichier, HttpStatus.OK);	
	}
	
	@DeleteMapping("/fichier/{id}")
	public ResponseEntity<List<Fichier>> supprimer(@PathVariable(value = "id") int id) {
		Fichier fichier=new Fichier();
		Fichier fichier2 = new Fichier();
		fichier = fichierRepository.getOne(id);
		fichier2.setId(id);
		try {
			Files.deleteIfExists(Paths.get(fichier.getPath()));
			this.fichierRepository.delete(fichier2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Fichier>lsfichier=fichierRepository.findAll();
		return new ResponseEntity<List<Fichier>>(lsfichier, HttpStatus.OK);	
	}

	@PostMapping("/fichier")
	public ResponseEntity<String> uploadData(@RequestParam("file") MultipartFile file) throws Exception {
		if (file == null) {
			throw new RuntimeException("You must select the a file for uploading");
		}
		InputStream inputStream = file.getInputStream();
		String originalName = file.getOriginalFilename();
		String name = file.getName();
		String contentType = file.getContentType();
		
		byte[] bytes = file.getBytes();
        Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
        Files.write(path, bytes);
        
        Fichier fichier = new Fichier();
        fichier.setNom_fichier(file.getOriginalFilename());
        fichier.setPath(UPLOADED_FOLDER + file.getOriginalFilename());
		this.fichierRepository.save(fichier);
		
		// Do processing with uploaded file data in Service layer
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}
	
	@GetMapping("/fichier/{id}")
    public ResponseEntity<Fichier> getFichierById(@PathVariable(value = "id") int id)
        throws ResourceNotFoundException {
        Fichier fichier = fichierRepository.findById(id)
          .orElseThrow(() -> new ResourceNotFoundException("Fichier not found for this id :: " + id));
        return new ResponseEntity<Fichier>(fichier, HttpStatus.OK);
    }
	
	 @GetMapping("/fichier/download/{id}")
	 public ResponseEntity<Resource> download(@PathVariable(value = "id") int id) throws IOException {
		 Fichier fichier = fichierRepository.findById(id)
		          .orElseThrow(() -> new ResourceNotFoundException("Fichier not found for this id :: " + id));
		 Path path = Paths.get(fichier.getPath());
		 Resource resource = null;
			try {
				resource = new UrlResource(path.toUri());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			return ResponseEntity.ok()
					.contentType(MediaType.parseMediaType("application/pdf"))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);
		 
	 }
	 
	 @PutMapping(value="/fichier/{id}")
	 public ResponseEntity<List<Fichier>> update(@PathVariable(value = "id") int id,@RequestBody Fichier fichierDetails) {
		 Fichier fichier = fichierRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Fichier not found for this id :: " + id));
		 fichier.setNom_fichier(fichierDetails.getNom_fichier());
		 fichier.setPath(fichierDetails.getPath());
		 File target = new File(fichierDetails.getPath());
		 File rename = new File(UPLOADED_FOLDER + fichierDetails.getNom_fichier());
		 if(target.renameTo(rename)) {
	            System.out.println("File was successfully renamed");
	        } else {
	            System.out.println("Error: Unable to rename file");
	        }
			this.fichierRepository.save(fichier);
			List<Fichier>lsfichier=fichierRepository.findAll();
			return new ResponseEntity<List<Fichier>>(lsfichier, HttpStatus.OK);	
	}
	
}
