package com.crud.crud.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crud.crud.Dossier;
import com.crud.crud.Utilisateurs;
import com.crud.crud.dao.UtilisateurRepository;

@Controller
@CrossOrigin("*")
@RequestMapping("/dematerialisation")
public class UtilisateurController {
	@Autowired
private UtilisateurRepository utilisateurRepository;
	
	@GetMapping(value="/utilisateurs")
	public ResponseEntity<List<Utilisateurs>> liste() {
		List<Utilisateurs>utilisateurs=utilisateurRepository.findAll();
		
		return new ResponseEntity<List<Utilisateurs>>(utilisateurs, HttpStatus.OK);	
	}
	
	@GetMapping("/utilisateurs/{id}")
    public ResponseEntity<Utilisateurs> getUtilisateurById(@PathVariable(value = "id") int id)
        throws ResourceNotFoundException {
        Utilisateurs utilisateur = utilisateurRepository.findById(id)
          .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));
        return new ResponseEntity<Utilisateurs>(utilisateur, HttpStatus.OK);
    }
	
	@DeleteMapping("/utilisateurs/{id}")
	public ResponseEntity<List<Utilisateurs>> supprimer(@PathVariable(value = "id") int id) {
		Utilisateurs utilisateurs=new Utilisateurs();
		utilisateurs.setId(id);
		this.utilisateurRepository.delete(utilisateurs);
		List<Utilisateurs>lsutilisateurs=utilisateurRepository.findAll();
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateurs, HttpStatus.OK);	
	}
	
	@PostMapping(value="/utilisateurs")
	public ResponseEntity<List<Utilisateurs>> ajout(@RequestBody Utilisateurs utilisateurs) {
		
		this.utilisateurRepository.save(utilisateurs);
		List<Utilisateurs>lsutilisateurs=utilisateurRepository.findAll();
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateurs, HttpStatus.OK);	
	}
	
	@PutMapping(value="/utilisateurs/{id}")
	public ResponseEntity<List<Utilisateurs>> update(@PathVariable(value = "id") int id,
	         @RequestBody Utilisateurs utilisateurDetails) {
        Utilisateurs utilisateur = utilisateurRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));
        
        utilisateur.setNom(utilisateurDetails.getNom());
        utilisateur.setPrenom(utilisateurDetails.getPrenom());
        utilisateur.setMatricule(utilisateurDetails.getMatricule());
        utilisateur.setAdresse(utilisateurDetails.getAdresse());
        utilisateur.setMail(utilisateurDetails.getMail());
        utilisateur.setTelephone(utilisateurDetails.getTelephone());
        utilisateur.setService(utilisateurDetails.getService());
        utilisateur.setFonction(utilisateurDetails.getFonction());
        utilisateur.setMot_de_passe_identifiant(utilisateurDetails.getMot_de_passe_identifiant());
        
		this.utilisateurRepository.save(utilisateur);
		List<Utilisateurs>lsutilisateurs=utilisateurRepository.findAll();
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateurs, HttpStatus.OK);	
	}
	
	
	@RequestMapping(value="/rechercheUtilisateurs")
	public ResponseEntity<List<Utilisateurs>> rechercherUtilisateurs(@RequestParam(name="nom",defaultValue = "" )String nom) {
		
		List<Utilisateurs> lsutilisateur= this.utilisateurRepository.findlikeNom("%"+nom+"%");
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateur, HttpStatus.OK);	
}
}
