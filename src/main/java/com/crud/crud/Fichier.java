package com.crud.crud;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Fichier implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private int id;
	@Column
	private String nom_fichier;
	@Column
	private String path;
	
	
	public Fichier() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Fichier(String nom_fichier, String path) {
		super();
		this.nom_fichier = nom_fichier;
		this.path = path;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom_fichier() {
		return nom_fichier;
	}

	public void setNom_fichier(String nom_fichier) {
		this.nom_fichier = nom_fichier;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
