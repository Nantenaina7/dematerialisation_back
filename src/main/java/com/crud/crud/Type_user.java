package com.crud.crud;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Type_user implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	
	
@Column
private int id;
@Column
private String nom_type_user;

@JsonIgnore
@OneToMany(mappedBy="type_user",fetch=FetchType.LAZY)
private Collection <Utilisateurs> utilisateurs;

public Type_user(String nom_type_user) {
	super();
	this.nom_type_user = nom_type_user;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNom_type_user() {
	return nom_type_user;
}

public Type_user() {
	super();
	// TODO Auto-generated constructor stub
}

public void setNom_type_user(String nom_type_user) {
	this.nom_type_user = nom_type_user;
}

}
